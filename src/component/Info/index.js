import React from 'react'
import './info-style.scss'
import config from '../../../config'

const Info = props => (
    <section
        data-id="info"
        onClick={props.onClick}
        >
        <div>
            <h2>{config.text.title}</h2>
            <p>
                {config.text.desc}
            </p>
            <p>
                {config.text.creditDesc}
            </p>
            <p>
                <a href="http://goatstone.com" target="new">{config.text.credit}</a>&nbsp;{config.text.copyright}
            </p>
            <p>
                {config.text.dataCredit}&nbsp;
                <a href="https://www.cryptocompare.com" target="new">{config.text.dataCreditLinkText}</a>
            </p>
            <ul>
                <li>
                    <a href="/terms.html" target="terms">{config.text.links.terms}</a>
                </li>
                <li>
                    <a href="/privacy.html" target="privacy">{config.text.links.privacy}</a>
                </li>
            </ul>
        </div>
    </section>
)
export default Info
