import React from 'react'
import './style.scss'

const excludeProps = ['Id', 'Url', 'Name', 'FullName', 'ImageUrl', 'CoinName', 'SortOrder']
const ShortDesc = ({ obj }) => {
    const objArr = Object.entries(obj)
    const title = obj.FullName
    const els = objArr
        .filter(e => !excludeProps.includes(e[0]))
        .map((e) => {
            const v = (typeof e[1] === 'boolean') ? String(e[1]) : e[1]
            return [e[0], v]
        })
        .map((e) => {
            return (<div><dt>{e[0].replace(/([a-z])([A-Z])/g, '$1 $2')}</dt>
                <dd> {e[1] }</dd></div>)
        })
    return (
        <div data-id="desc-short">
            <dl>
                <dt data-id="desc.title">{title}</dt>
                {els}
            </dl ></div>)
}

export default ShortDesc
