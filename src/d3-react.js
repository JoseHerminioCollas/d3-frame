import EventEmitter from 'events'
import React from 'react'
import Info from './component/Info'
import './container.scss'
import config from '../config'
import ShortDesc from './component/ShortDesc'
import Logo from './component/Logo'
import axios from 'axios'
import coinData2 from '../data/coin-data2'

const events = new EventEmitter()

const dev = true
const coin = coinData2.Data
const dataLocal = Object.entries(coin)
// https://miracoin.vision/coin
const urlName = 'https://pepple/coin'
// const BaseImageUrl = coinData.BaseLinkUrl

class D3React extends React.Component {
    constructor() {
        super(...Array.from(arguments))
        this.state = {
            focusIndex: 0,
            data: [],
            selectedCoin: {},
            isInfoVissible: false,
        }
        this.inputs = []
        this.onFocus = this.symbolFocus
        this.onFocus = this.onFocus.bind(this)
        this.controlEvent = events
        // create a ref to store the textInput DOM element
        this.textInput = React.createRef()
        this.focusTextInput = this.focusTextInput.bind(this)
        this.resetFocus = true
        this.hideInfo = this.hideInfo.bind(this)
        this.dataIsLoaded = false
        this.setEvents()
    }
    setData() {
        const that = this
        if (dev) {
            this.dataIsLoaded = true
            that.setState({
                data: dataLocal,
                selectedCoin: dataLocal[0][1],
            })
            return
        } else {
            axios.get(urlName)
                .then(function (response) {
                    const d = Object.entries(response.data.Data)
                    that.dataIsLoaded = true
                    that.setState({
                        data: d,
                        selectedCoin: d[0][1],
                    })
                })
                .catch(function (error) {
                    console.log('err', error);
                })
        }
    }
    focusTextInput() {
        // Explicitly focus the text input using the raw DOM API
        // Note: we're accessing "current" to get the DOM node
        this.textInput.current.focus()
    }
    hideInfo() {
        this.setState({ isInfoVissible: false })
    }
    emitEvent(e) {
        e.preventDefault()
        const { name, value } = e.target
        events.emit(name, value)
    }
    setEvents() {
        this.controlEvent.on('info', () => {
            this.setState({ isInfoVissible: true })
        })
    }
    symbolFocus = (e) => {
        this.setState({ selectedCoin: e })
    }
    componentDidMount() {
        // setTimeout(() => this.setData(), 1000)
        this.setData()
    }
    componentDidUpdate() {
        const that = this
        if (this.resetFocus) {
            console.log('did update', new Date(), this)
            this.textInput.current.focus()
            return false;
        }
    }
    render() {
        const titleColors = ['orange', '#ccc', '#ccc', '#ccc', '#ccc', '#ccc', '#ccc', 'orange']
        return (
            <section data-id="container">

                {!this.dataIsLoaded && (<div style={{ fontSize: '4rem' }}>
                    Loading
                    </div>)
                }

                {this.state.data.map((e, i) => {
                    return (
                        <article
                            ref={i === 0 && this.textInput}
                            data-name={e[1].Name}
                            key={e[0]}
                            tabIndex={i}
                            onFocus={() => this.onFocus(e[1])}>
                            {e[1].Name}
                        </article>
                    )
                })}

                <div data-id="info-blurb">
                    <div>
                        <h1>{config.text.title.split('').map((l, i) => (
                            <span style={{ color: titleColors[i] }}>
                                {l}
                            </span>))}
                        </h1>
                        <Logo
                            style={config.style.logoA}
                            />
                        <span>{config.text.shortDesc}</span>
                    </div>
                    <div>
                        {config.text.dataCredit}&nbsp;
                        <a href="https://www.cryptocompare.com" target="new">{config.text.dataCreditLinkText}</a>
                    </div>
                    <div>
                        <span>
                            <a href="http://goatstone.com" target="new">{config.text.credit}</a>
                            &nbsp;{config.text.copyright}
                        </span>
                        <button name="info" value="show" onClick={this.emitEvent}>
                            ?
                        </button>
                    </div>
                </div>
                <ShortDesc
                    obj={this.state.selectedCoin}
                    />
                {this.state.isInfoVissible &&
                    <Info onClick={this.hideInfo} />
                }
            </section>)
    }
}
export default D3React
